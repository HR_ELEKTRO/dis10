# DIS10 - Digitale Systemen 1 #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om aanvullend studiemateriaal voor de cursus "DIS10 - Digitale Systemen 1" te verspreiden.

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/dis10/wiki/).